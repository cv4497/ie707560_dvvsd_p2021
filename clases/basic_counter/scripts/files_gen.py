import glob
import os

os.chdir("..")
os.chdir("src")

srcfiles = []
for file in glob.glob("*.sv"):
    srcfiles.append(file)

os.chdir("..")
os.chdir("tb")
    
tb_files = []
for file in glob.glob("*.sv"):
    tb_files.append(file)

f = open("files.f", "w")

pkg_files = []
src_files = []
top_module = []

for file in srcfiles:
    if("pkg.sv" in file):
        pkg_files.append(file)
    elif("top.sv" in file):
        top_module.append(file)
    else:
        src_files.append(file)

for file in pkg_files:
    print(file)
    f.write('../src/'+file+'\n')

for file in src_files:
    print(file)
    f.write('../src/'+file+'\n')

for file in top_module:
    print(file)
    f.write('../src/'+file+'\n')

for file in tb_files:
    print(file)
    f.write(file +'\n')

f.close()

