/* 
    Author: César Villarreal @cv4497
    Title:  Binary counter package
    Description: Package for binary counter with overflow
    Last modification: 25 January 2021
*/

`ifndef BASIC_COUNTER_PKG_SV
    `define BASIC_COUNTER_PKG_SV
package basic_counter_pkg;
    localparam COUNT_SIZE = 4;
    localparam MAX_COUNT = (2**COUNT_SIZE)-1;

    typedef logic [COUNT_SIZE-1:0] data_t;
endpackage
`endif
