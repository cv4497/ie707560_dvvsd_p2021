/* 
    Author: César Villarreal @cv4497
    Title:  Binary counter testbench
    Description: testbench for binary counter with overflow
    Last modification: 25 January 2021
*/

`timescale 1ns / 1ps
module basic_counter_tb();
import basic_counter_pkg::*;
logic clk;
logic rst;
logic enb;
logic ovf;
data_t count;

localparam PERIOD = 2;

basic_counter dut
(
    .clk(clk),
    .rst(rst),
    .enb(enb),
    .ovf(ovf),
    .count(count)
);

initial begin
             clk = 1'd0; 
             rst = 1'd1;
             enb = 1'd1;
    #PERIOD  rst = 1'd0;
    #PERIOD  rst = 1'd1;
end

always begin
    #(PERIOD/2) clk<= ~clk;
end

endmodule