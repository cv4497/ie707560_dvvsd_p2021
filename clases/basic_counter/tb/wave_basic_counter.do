onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -label clk /basic_counter_tb/dut/clk
add wave -noupdate -color Gray90 -label rst /basic_counter_tb/dut/rst
add wave -noupdate -color {Cornflower Blue} -label enb /basic_counter_tb/dut/enb
add wave -noupdate -color Gold -label ovf /basic_counter_tb/dut/ovf
add wave -noupdate -label count -radix unsigned /basic_counter_tb/dut/count
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {36207 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 290
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {1662 ps} {47486 ps}
