onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -label bin_data -radix decimal /binary_bcd_dec_tb/dut/bin2bcd/bin_data
add wave -noupdate -expand -group {BINARY 2 BCD} -label bcd_hundreds -radix unsigned /binary_bcd_dec_tb/dut/bin2bcd/bcd_hundreds
add wave -noupdate -expand -group {BINARY 2 BCD} -label bcd_tens -radix unsigned /binary_bcd_dec_tb/dut/bin2bcd/bcd_tens
add wave -noupdate -expand -group {BINARY 2 BCD} -label bcd_units -radix unsigned /binary_bcd_dec_tb/dut/bin2bcd/bcd_units
add wave -noupdate -expand -group {BINARY 2 BCD} -label bcd_sign /binary_bcd_dec_tb/dut/bin2bcd/bcd_sign
add wave -noupdate -expand -group hundreds_seg_decoder -label i_data /binary_bcd_dec_tb/dut/hundreds_seg_decoder/i_data
add wave -noupdate -expand -group hundreds_seg_decoder -label o_data /binary_bcd_dec_tb/dut/hundreds_seg_decoder/o_data
add wave -noupdate -expand -group tens_seg_decoder -label i_data /binary_bcd_dec_tb/dut/tens_seg_decoder/i_data
add wave -noupdate -expand -group tens_seg_decoder -label o_data /binary_bcd_dec_tb/dut/tens_seg_decoder/o_data
add wave -noupdate -expand -group units_seg_decoder -label i_data /binary_bcd_dec_tb/dut/units_seg_decoder/i_data
add wave -noupdate -expand -group units_seg_decoder -label o_data /binary_bcd_dec_tb/dut/units_seg_decoder/o_data
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {1303 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 150
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {0 ps} {20168 ps}
