 /*
	Coded by: César Villarreal @cv4497
	Date: August 31, 2020
	Description: test-bench for binary decoder
 */

`timescale 1ns / 1ps
module binary_bcd_dec_tb();
import binary_bcd_pkg::*;

sw_data_t bin_data;
seg_data_t seg_hundreds;
seg_data_t seg_tens;
seg_data_t seg_units;
logic seg_sign;

binary_2_bcd_top dut
(
	.bin_data(bin_data),
    .seg_hundreds(seg_hundreds),
    .seg_tens(seg_tens),
    .seg_units(seg_units),
    .seg_sign(seg_sign)
);

    initial begin
        /* counter from 0 to 15 */
        bin_data = 'd0;
        repeat (15)
        #1 bin_data = bin_data + 'd1;
        /* test positive data */
        #1 bin_data = 'd68;
    	#1 bin_data = 'd34;
        #1 bin_data = 'd127;
        /* counter from 0 to -15 */
        #1 bin_data = 'd0;
        repeat (15)
        #1 bin_data = bin_data - 'd1;
        /* test negative data */
    	#1 bin_data = -'d128;

    $stop;
end

endmodule
