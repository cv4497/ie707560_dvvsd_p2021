 /*
	Coded by: César Villarreal @cv4497
	Date: January 19, 2021
	Description: this module converts binary coded data (CBD) to seven segment display data
 */

module seven_segments_decoder
import binary_bcd_pkg::*;
(
	input  sw_data_t i_data,
	output seg_data_t o_data
);
			
always_comb begin 				  
	o_data =  (i_data == 4'd0)  ? 7'b1000000: 
			    (i_data == 4'd1)  ? 7'b1111001: 
			    (i_data == 4'd2)  ? 7'b0100100:   
			    (i_data == 4'd3)  ? 7'b0110000: 
			    (i_data == 4'd4)  ? 7'b0011001:  
			    (i_data == 4'd5)  ? 7'b0010010: 
			    (i_data == 4'd6)  ? 7'b0000010:  
			    (i_data == 4'd7)  ? 7'b1111000:  
			    (i_data == 4'd8)  ? 7'b0000000:   
			    (i_data == 4'd9)  ? 7'b0011000: 
				 (i_data == 4'd10) ? 7'b0001000:
				 (i_data == 4'd11) ? 7'b0000011:
				 (i_data == 4'd12) ? 7'b1000110:
				 (i_data == 4'd13) ? 7'b0100001:
				 (i_data == 4'd14) ? 7'b0000110:
				 (i_data == 4'd15) ? 7'b0001110:
			   						   7'b1000000;
end

endmodule