 /*
	Coded by: César Villarreal
	Date: January 19, 2021
	Description: Binary Decoder (top-module)
 */

module binary_2_bcd_top
import binary_bcd_pkg::*;
(
	input  sw_data_t bin_data,       //binary inputs         - SW[0:7]
	output seg_data_t seg_hundreds,  //segments for hundreds - HEX2[0:6]
	output seg_data_t seg_tens,      //segments for tens     - HEX1[0:6]
	output seg_data_t seg_units,     //segments for units    - HEX0[0:6]
	output logic seg_sign            //sign indicator        - HEX3[6]
);

/* wire declarations */
bcd_data_t hundreds_bcd_2_seg_w;
bcd_data_t tens_bcd_2_seg_w;
bcd_data_t units_bcd_2_seg_w;

/* 
	Description: binary to bcd converter module
	Inputs:  bcd_data
	Outputs: segments 
*/
binary_bcd_decoder bin2decimal
(
	.bin_data(bin_data),
	.bcd_hundreds(hundreds_bcd_2_seg_w),
	.bcd_tens(tens_bcd_2_seg_w),
	.bcd_units(units_bcd_2_seg_w),
	.bcd_sign(seg_sign)
);

/* 
	Description: BCD to segments decoder for hundreds
	Inputs:  bcd_data
	Outputs: segments 
*/
seven_segments_decoder hundreds_seg_decoder
(
	.i_data(hundreds_bcd_2_seg_w),
	.o_data(seg_hundreds)
);

/* 
	BCD to segments decoder for tens
	Inputs:  bcd_data
	Outputs: segments 
*/
seven_segments_decoder tens_seg_decoder
(
	.i_data(tens_bcd_2_seg_w),
	.o_data(seg_tens)
);

/* 
	BCD to segments decoder for units
	Inputs:  bcd_data
	Outputs: segments 
*/
seven_segments_decoder units_seg_decoder
(
	.i_data(units_bcd_2_seg_w),
	.o_data(seg_units)
);

endmodule