/*
	Coded by: César Villarreal
	Date: January 19, 2021
	Description: Binary Decoder (top-module)
*/
 
module binary_bcd_decoder
import binary_bcd_pkg::*;
(
	input  bin_data_t bin_data,
	output bcd_data_t bcd_hundreds,
	output bcd_data_t bcd_tens,
	output bcd_data_t bcd_units,
	output logic bcd_sign
);

bin_data_t c2bin_data;

assign bcd_sign = (bin_data[7] == 1'b0)? 1'b1: 1'b0;
assign c2bin_data = (bin_data[7] == 1'b0)? bin_data:(~bin_data + 1'b1);

assign bcd_hundreds = ((c2bin_data*'d10) >> 'd10);
assign bcd_tens = (((c2bin_data - (bcd_hundreds*'d100))*'d102) >> 'd10);
assign bcd_units = c2bin_data - (bcd_hundreds*'d100) - (bcd_tens*'d10);

endmodule