 /*
	Coded by: César Villarreal
	Date: January 19, 2021
	Description: this package contains all data type definitions
 */
`ifndef binary_bcd_dec_pkg_sv
`define binary_bcd_dec_pkg_sv

package binary_bcd_pkg;

	localparam sw_size = 8;
	localparam bcd_output_len = 4;
	localparam SEG_SIZE = 7;

	typedef logic [sw_size-1:0] sw_data_t;
	typedef logic [sw_size-1:0] bin_data_t;
	typedef logic [bcd_output_len-1:0] bcd_data_t;
	typedef logic [SEG_SIZE-1:0] seg_data_t;
				 
endpackage

`endif
 
