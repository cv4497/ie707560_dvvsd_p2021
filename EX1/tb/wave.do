onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -label clk /fir_tb/FILTER/clk
add wave -noupdate -label rst /fir_tb/FILTER/rst
add wave -noupdate -label enb /fir_tb/FILTER/enb
add wave -noupdate -label X -radix decimal /fir_tb/FILTER/X
add wave -noupdate -label Y -radix decimal /fir_tb/FILTER/Y
add wave -noupdate -radix decimal /fir_tb/FILTER/B0
add wave -noupdate -radix decimal /fir_tb/FILTER/B1
add wave -noupdate -radix decimal /fir_tb/FILTER/B2
add wave -noupdate -radix decimal /fir_tb/FILTER/B3
add wave -noupdate -radix decimal /fir_tb/FILTER/B4
add wave -noupdate /fir_tb/FILTER/MULT0/A
add wave -noupdate /fir_tb/FILTER/MULT0/B
add wave -noupdate /fir_tb/FILTER/MULT0/result
add wave -noupdate /fir_tb/FILTER/Y
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {293 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 105
configure wave -valuecolwidth 40
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {0 ps} {345 ps}
