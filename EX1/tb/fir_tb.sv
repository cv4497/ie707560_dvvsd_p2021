
module fir_tb;
    import fir_filter_pkg::*;
    localparam PERIOD = 2;

    logic clk;
    logic rst;
    logic enb;
    data_t X;
    data_t Y;

    fir_filter FILTER
    (
        .clk(clk),
        .rst(rst),
        .enb(enb),
        .X(X),
        .Y(Y)
    );

    initial begin
        #PERIOD clk = 0;
        #PERIOD rst = 1;
        #PERIOD enb = 0;  
        #PERIOD X = 14'b0000000001000000;
        #PERIOD 
        #PERIOD enb = 1; 
        #500;
        $stop;
    end


    always begin
        #(PERIOD/2)clk <= ~clk;
    end

endmodule