
import fir_filter_pkg::*;

module fir_filter
(
    input clk,
    input rst,
    input logic enb,
    input  data_t X,
    output data_t Y
);

coeffient_t B0;
coeffient_t B1;
coeffient_t B2;
coeffient_t B3;
coeffient_t B4;
coeffient_t B5;
coeffient_t B6;
coeffient_t B7;
coeffient_t B8;
coeffient_t B9;
coeffient_t B10;
coeffient_t B11;
coeffient_t B12;
coeffient_t B13;

data_t pipo_reg_out;
count_t count;
data_t adder_result_w;
logic[1:0] mult_res0_w;
logic[1:0] mult_res1_w;
logic[1:0] mult_res2_w;
logic[1:0] mult_res3_w;
logic[1:0] mult_res4_w;
logic[1:0] mult_res5_w;
logic[1:0] mult_res6_w;
logic[1:0] mult_res7_w;
logic[1:0] mult_res8_w;
logic[1:0] mult_res9_w;
logic[1:0] mult_res10_w;
logic[1:0] mult_res11_w;
logic[1:0] mult_res12_w;
logic[1:0] mult_res13_w;

pipo_register 
#(
    .DW(fir_filter_pkg::N)
)   sample_register
(
    .clk(clk),
    .rst(rst),
    .enb(enb),
    .i_data(X),
    .o_data(pipo_reg_out)
);


multiplier MULT0
(
    .A(pipo_reg_out[0]),
    .B(B0),
    .result(mult_res0_w)
);

multiplier MULT1
(
    .A(pipo_reg_out[1]),
    .B(B1),
    .result(mult_res1_w)
);

multiplier MULT2
(
    .A(pipo_reg_out[2]),
    .B(B2),
    .result(mult_res2_w)
);

multiplier MULT3
(
    .A(pipo_reg_out[3]),
    .B(B3),
    .result(mult_res3_w)
);

multiplier MULT4
(
    .A(pipo_reg_out[4]),
    .B(B4),
    .result(mult_res4_w)
);

multiplier MULT5
(
    .A(pipo_reg_out[5]),
    .B(B5),
    .result(mult_res5_w)
);

multiplier MULT6
(
    .A(pipo_reg_out[6]),
    .B(B6),
    .result(mult_res6_w)
);

multiplier MULT7
(
    .A(pipo_reg_out[7]),
    .B(B7),
    .result(mult_res7_w)
);

multiplier MULT8
(
    .A(pipo_reg_out[8]),
    .B(B8),
    .result(mult_res8_w)
);

multiplier MULT9
(
    .A(pipo_reg_out[9]),
    .B(B9),
    .result(mult_res9_w)
);

multiplier MULT10
(
    .A(pipo_reg_out[10]),
    .B(B10),
    .result(mult_res10_w)
);

multiplier MULT11
(
    .A(pipo_reg_out[11]),
    .B(B11),
    .result(mult_res11_w)
);

multiplier MULT12
(
    .A(pipo_reg_out[12]),
    .B(B12),
    .result(mult_res12_w)
);

multiplier MULT13
(
    .A(pipo_reg_out[13]),
    .B(B13),
    .result(mult_res13_w)
);


always_comb begin
    B0 = 1;
    B1 = -1;
    B2 = 1;
    B3 = -1;
    B4 = 1;
    B5 = -1;
    B6 = 1;
    B7 = -1;

    Y = mult_res0_w+mult_res1_w+mult_res2_w+mult_res3_w+mult_res4_w+mult_res5_w+mult_res6_w+mult_res7_w+mult_res8_w+mult_res9_w+mult_res10_w+mult_res11_w+mult_res12_w+mult_res13_w;
end


endmodule