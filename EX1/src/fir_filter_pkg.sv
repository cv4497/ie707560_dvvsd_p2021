
package fir_filter_pkg;
localparam N = 14;

typedef logic [N-1:0] data_t;
typedef logic [1:0] coeffient_t;

typedef enum logic [3:0]
{
    NO_OP = 2'b00,
    ADDITION = 2'b01,
    SUBSTRACTION = 2'b10,
    NO_OP2 = 2'b11
}alu_op_sel_str;

typedef logic [3:0]  count_t;

endpackage