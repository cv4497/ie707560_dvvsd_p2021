

module multiplier
import fir_filter_pkg::*;
#(
	parameter DW = fir_filter_pkg::N
)
(
    input logic A,
    input coeffient_t B,
    output logic[1:0] result
);

assign result = A*B;

endmodule