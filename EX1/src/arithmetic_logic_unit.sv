/* 
    Authors: Cesar Villarreal @cv4497
             Luis Fernando Rodriguez @LF-RoGu
    Title: arithmetic_logic_unit
    Description: arithmetic_logic_unit source file
    Last modification: 14/02/2021
*/

import fir_filter_pkg::*;
module adder
(
	/** INPUT*/
	input logic A,
	input logic B,
	input logic C,
	input logic D,
	input logic E,
	output data_t result
);
	 
always_comb begin
	result = A+B+C+D+E;
end 	

endmodule 