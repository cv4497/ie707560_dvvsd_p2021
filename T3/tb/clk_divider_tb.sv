 /*
	Coded by: César Villarreal @cv4497
	Date: August 31, 2020
	Description: test-bench for binary decoder
 */

`timescale 1ns / 1ps
module clk_divider_tb();
import clk_divider_pkg::*;

logic       clk;
logic       rst;
logic       o_clk;

clk_divider clk_div
(
    .clk(clk),
    .rst(rst),
    .o_clk(o_clk)
);

initial begin
        clk = 0;
        rst = 0;
    #2  rst = 1;  
    #100
    $stop;
end

always begin
    #1 clk <= ~clk;
end

endmodule
