/* 
    Author: César Villarreal @cv4497
    Title: Clock Divider
    Description: this is the top-module for the clock divider project.
    Last modification: 07/09/2020
*/

module clk_divider_top
(
	input   clk,
   input   rst,
   output  pin_out
);

/* wire declaration */
logic clkdiv_w;

/* module: clock divider (clk_div)
   input:  clk_FPGA, rst
   output: clk
*/
clk_divider clk_div
(
   .clk(clk),
   .rst(rst),
   .o_clk(clkdiv_w)
);

assign pin_out = clkdiv_w;

endmodule