/* 
    Author: César Villarreal @cv4497
    Title: Clock Divider
    Description: this module contains the implementation of a frequency divider.
                 the output frequency (clk) can be changed according to its parameters.
    Last modification: 07/09/2020
*/

module clk_divider
import clk_divider_pkg::*;
#(
    parameter FREQUENCY=1,            //input frequency
    parameter REFERENCE_CLOCK=1000 //50 MHz clock
)(
	input   bit clk,
    input   bit rst,
    output  bit o_clk
);

counter_t count_r;
counter_t cnt_nxt;
logic clk_r;

/* Combinational process */
always_comb begin
  cnt_nxt  = count_r + 1'b1;
end

/* Sequential process */
always_ff@(posedge clk, negedge rst) begin
    if (!rst)
	 begin
        count_r	<=  '0;
		  clk_r <= 1'b0;
	 end
    else
		 begin
			if(count_r > (REFERENCE_CLOCK/(FREQUENCY))-1)
					count_r <= '0;
			else
					count_r	<=  cnt_nxt;
					
			if(count_r < ((REFERENCE_CLOCK/(FREQUENCY*2)))-1)
					clk_r <= 1'b1;
			else
					clk_r <= 1'b0;
		 end
		  
end

assign o_clk = clk_r;

endmodule