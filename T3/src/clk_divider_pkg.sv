 /*
    Author: César Villarreal @cv4497
    Title: Clock Divider
    Description: this package contains the data definitions 
				 for the clock divider.
    Last modification: 07/09/2020
 */
`ifndef clk_divider_pkg_sv
`define clk_divider_pkg_sv

package clk_divider_pkg;

   localparam N = 27;  //number of counters
   typedef logic [N-1:0] counter_t;

endpackage

`endif
 
