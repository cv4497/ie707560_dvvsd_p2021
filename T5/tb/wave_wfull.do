onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -expand -group {write full} -label clk /write_full_tb/uut/clk
add wave -noupdate -expand -group {write full} -label rst /write_full_tb/uut/rst
add wave -noupdate -expand -group {write full} -label inc /write_full_tb/uut/inc
add wave -noupdate -expand -group {write full} -label rptr /write_full_tb/uut/rptr
add wave -noupdate -expand -group {write full} -label waddr /write_full_tb/uut/waddr
add wave -noupdate -expand -group {write full} -label wptr /write_full_tb/uut/wptr
add wave -noupdate -expand -group {write full} -label wgraynext /write_full_tb/uut/gray_count/graynext
add wave -noupdate -expand -group {write full} -label full_flag /write_full_tb/uut/full_flag
add wave -noupdate -expand -group {write full} -group {ver full} -label ptr_1 /write_full_tb/uut/ver_full/ptr_1
add wave -noupdate -expand -group {write full} -group {ver full} -label ptr_2 /write_full_tb/uut/ver_full/ptr_2
add wave -noupdate -expand -group {write full} -group {ver full} -label full_flag /write_full_tb/uut/ver_full/full_flag
add wave -noupdate -expand -group {write full} -group {Gray Counter} -color {Sea Green} -label clk /write_full_tb/uut/gray_count/clk
add wave -noupdate -expand -group {write full} -group {Gray Counter} -color White -label rst /write_full_tb/uut/gray_count/rst
add wave -noupdate -expand -group {write full} -group {Gray Counter} -color Gold -label inc /write_full_tb/uut/gray_count/inc
add wave -noupdate -expand -group {write full} -group {Gray Counter} -color Gold -label not_full_or_empty /write_full_tb/uut/gray_count/not_full_or_empty
add wave -noupdate -expand -group {write full} -group {Gray Counter} -group bin2gray -label binary /write_full_tb/uut/gray_count/bin2gray/binary
add wave -noupdate -expand -group {write full} -group {Gray Counter} -group bin2gray -label gray /write_full_tb/uut/gray_count/bin2gray/gray
add wave -noupdate -expand -group {write full} -group {Gray Counter} -group binary_reg -label enable /write_full_tb/uut/gray_count/binary_reg/enable
add wave -noupdate -expand -group {write full} -group {Gray Counter} -group binary_reg -label D /write_full_tb/uut/gray_count/binary_reg/D
add wave -noupdate -expand -group {write full} -group {Gray Counter} -group binary_reg -label Q /write_full_tb/uut/gray_count/binary_reg/Q
add wave -noupdate -expand -group {write full} -group {Gray Counter} -group gray_reg -label enable /write_full_tb/uut/gray_count/gray_reg/enable
add wave -noupdate -expand -group {write full} -group {Gray Counter} -group gray_reg -label D /write_full_tb/uut/gray_count/gray_reg/D
add wave -noupdate -expand -group {write full} -group {Gray Counter} -group adder -label A /write_full_tb/uut/gray_count/add/A
add wave -noupdate -expand -group {write full} -group {Gray Counter} -group adder -label B /write_full_tb/uut/gray_count/add/B
add wave -noupdate -expand -group {write full} -group {Gray Counter} -group adder -label result /write_full_tb/uut/gray_count/add/result
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {9 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 150
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {499955 ps} {500003 ps}
