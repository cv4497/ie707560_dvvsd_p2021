
timeunit 1ps; //It specifies the time unit that all the delay will take in the simulation.
timeprecision 1ps;// It specifies the resolution in the simulation.

module pointers_tb;
import fifo_pkg::*;

// Parameter Declarations
parameter Word_Length = 16;
parameter Depth_Of_FIFO = 8;
localparam PERIOD = 2;
localparam WFRQ = 60000;
localparam RFRQ = 40000;

/*******************************************************/
bit clk;
bit wclk;
bit rclk;
bit rst;

logic full_flag;
logic empty_flag;
logic winc;
logic rinc;
logic [sdp_dc_ram_pkg::W_DATA-1:0] wdata;
logic push;
logic pop;
logic disable_we;


/*******************************************************/
ptr_t owptr_iffd1wr_w;
ptr_t offd1wr_iffd2wr_w;
ptr_t offd2wr_irptr_w;

ptr_t orptr_iffd1rd_w;
ptr_t offd1rd_iffd2rd_w;
ptr_t offd2rd_iwptr_w;

addr_t waddr;
addr_t raddr;

logic disable_rd;
logic mem_written;
logic push_rptr;
logic start_rptr;

sdp_dc_ram_if mem_if(); 

sdp_dc_ram  ram(
    .clk_a(wclk),
    .clk_b(rclk),
    .mem_if(mem_if.mem)
);

clk_divider
#(
    .FREQUENCY(WFRQ),
    .REFERENCE_CLOCK(50_000_000)
)   wclk_div
(
	.clk_FGPA(clk),
    .rst(rst),
    .clk(wclk)
);

clk_divider
#(
    .FREQUENCY(RFRQ),
    .REFERENCE_CLOCK(50_000_000)
)   rclk_div
(
	.clk_FGPA(clk),
    .rst(rst),
    .clk(rclk)
);

wptr_full wptr_full_and_inc
(
    .clk(wclk),
    .rst(rst),
    .inc(push),
    .full_flag(full_flag),
    .rptr(offd2rd_iwptr_w),
    .waddr(waddr),
    .wptr(owptr_iffd1wr_w)
);


/* 2FF SYNCHRONIZER FRM RCLK TO WCLK */
pipo_register sync_to_read_2ff_1
(
    .clk(rclk),
    .rst(rst),
    .enable(winc),
    .D(owptr_iffd1wr_w),
    .Q(offd1wr_iffd2wr_w)
);

pipo_register sync_to_read_2ff_2
(
    .clk(rclk),
    .rst(rst),
    .enable(winc),
    .D(offd1wr_iffd2wr_w),
    .Q(offd2wr_irptr_w)
);
/**************************************/

/* 2FF SYNCHRONIZER FRM WCLK TO RCLK */
pipo_register sync_to_write_2ff_2
(
    .clk(wclk),
    .rst(rst),
    .enable(rinc),
    .D(offd1rd_iffd2rd_w),
    .Q(offd2rd_iwptr_w)
);

pipo_register sync_to_write_2ff_1
(
    .clk(wclk),
    .rst(rst),
    .enable(rinc),
    .D(orptr_iffd1rd_w),
    .Q(offd1rd_iffd2rd_w)
);
/**************************************/

rptr_empty rptr_empty_and_inc
(
    .clk(rclk),
    .rst(rst),
    .inc(rinc),
    .wptr(offd2wr_irptr_w),
    .raddr(raddr),
    .rptr(orptr_iffd1rd_w),
    .empty_flag(empty_flag)
);

always_ff@(posedge rclk or negedge rst) begin
    if(!rst) begin
        mem_if.cln.rd_addr_b <= '0;
        disable_rd <= 1'b0;
        rinc <= 1'b0;
    end
    else if(pop) begin
        disable_rd <= 1'b1;
        rinc <= 1'b1;
        mem_if.cln.rd_addr_b <= raddr;
    end

    if(disable_rd == 1'b1) begin
        disable_rd <= 1'b0;
        rinc <= 1'b0;
    end
end

always_ff@(posedge wclk  or negedge rst) begin
    if(!rst) begin
        mem_if.cln.we_a <= 1'b0;
        mem_if.cln.wr_addr_a <= '0;
        mem_if.cln.data_a <= wdata;
        winc <= 1'b0;
    end
    else if(push) begin
        mem_if.cln.we_a <= 1'b1;
        mem_if.cln.wr_addr_a <= waddr;
        mem_if.cln.data_a <= wdata;
        disable_we <= 1'b1;
        winc <= 1'b1;
    end
    if(disable_we == 1'b1) begin
        disable_we <= 1'b0;
        winc <= 1'b0;
        mem_if.cln.we_a <= 1'b0;
    end
end

/******************** Stimulus *************************/

initial begin 
    #0 clk = 1'b0;
	#0 rst = 1'b0;
    #0 pop = 1'b0;
    #0 push = 1'b0;
    #0 wdata = 4'd5;
	#3 rst = 1'b1;
    #PERIOD rst = 1'b0;
    #PERIOD rst = 1'b1;
	
    repeat (16) begin
    #1666 push = 1'b1;
    #1666 push = 1'b0;
    #1666 wdata = wdata +1'b1;
    end


    repeat (3) begin
    #2500 pop = 1'b1;
    #2500 pop = 1'b0;
    end

    #20;
end 

always begin
    #(PERIOD/2) clk<= ~clk;
end

endmodule

 