onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -expand -group {Gray Counter} -color {Sea Green} -label clk /gray_counter_tb/uut/clk
add wave -noupdate -expand -group {Gray Counter} -color White -label rst /gray_counter_tb/uut/rst
add wave -noupdate -expand -group {Gray Counter} -color Gold -label inc /gray_counter_tb/uut/inc
add wave -noupdate -expand -group {Gray Counter} -color Gold -label not_full_or_empty /gray_counter_tb/uut/not_full_or_empty
add wave -noupdate -expand -group {Gray Counter} -color White -label {addr (binary) } /gray_counter_tb/addr
add wave -noupdate -expand -group {Gray Counter} -color Gray60 -label {ptr (gray)} /gray_counter_tb/ptr
add wave -noupdate -expand -group {Gray Counter} -group bin2gray -label binary /gray_counter_tb/uut/bin2gray/binary
add wave -noupdate -expand -group {Gray Counter} -group bin2gray -label gray /gray_counter_tb/uut/bin2gray/gray
add wave -noupdate -expand -group {Gray Counter} -group binary_reg -label enable /gray_counter_tb/uut/binary_reg/enable
add wave -noupdate -expand -group {Gray Counter} -group binary_reg -label D /gray_counter_tb/uut/binary_reg/D
add wave -noupdate -expand -group {Gray Counter} -group binary_reg -label Q /gray_counter_tb/uut/binary_reg/Q
add wave -noupdate -expand -group {Gray Counter} -group gray_reg -label enable /gray_counter_tb/uut/gray_reg/enable
add wave -noupdate -expand -group {Gray Counter} -group gray_reg -label D /gray_counter_tb/uut/gray_reg/D
add wave -noupdate -expand -group {Gray Counter} -group gray_reg -label Q /gray_counter_tb/uut/gray_reg/Q
add wave -noupdate -expand -group {Gray Counter} -group adder -label A /gray_counter_tb/uut/add/A
add wave -noupdate -expand -group {Gray Counter} -group adder -label B /gray_counter_tb/uut/add/B
add wave -noupdate -expand -group {Gray Counter} -group adder -label result /gray_counter_tb/uut/add/result
add wave -noupdate -color {Cornflower Blue} -label valid_count /gray_counter_tb/uut/valid_count
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {499987 ps} 0} {{Cursor 2} {0 ps} 0}
quietly wave cursor active 2
configure wave -namecolwidth 315
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {0 ps} {96 ps}
