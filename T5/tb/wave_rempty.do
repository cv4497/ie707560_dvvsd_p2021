onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -expand -group {Read Empty} -label clk /ready_empty_tb/uut/clk
add wave -noupdate -expand -group {Read Empty} -label rst /ready_empty_tb/uut/rst
add wave -noupdate -expand -group {Read Empty} -label inc /ready_empty_tb/uut/inc
add wave -noupdate -expand -group {Read Empty} -label rptr /ready_empty_tb/uut/rptr
add wave -noupdate -expand -group {Read Empty} -label wptr /ready_empty_tb/uut/wptr
add wave -noupdate -expand -group {Read Empty} -label wgraynext /ready_empty_tb/uut/gray_count/graynext
add wave -noupdate -expand -group {Read Empty} -label empty_flag /ready_empty_tb/uut/empty_flag
add wave -noupdate -expand -group {Read Empty} -expand -group {Gray Counter} -color {Sea Green} -label clk /ready_empty_tb/uut/gray_count/clk
add wave -noupdate -expand -group {Read Empty} -expand -group {Gray Counter} -color White -label rst /ready_empty_tb/uut/gray_count/rst
add wave -noupdate -expand -group {Read Empty} -expand -group {Gray Counter} -color Gold -label inc /ready_empty_tb/uut/gray_count/inc
add wave -noupdate -expand -group {Read Empty} -expand -group {Gray Counter} -color Gold -label not_full_or_empty /ready_empty_tb/uut/gray_count/not_full_or_empty
add wave -noupdate -expand -group {Read Empty} -expand -group {Gray Counter} -expand -group bin2gray -label binary /ready_empty_tb/uut/gray_count/bin2gray/binary
add wave -noupdate -expand -group {Read Empty} -expand -group {Gray Counter} -expand -group bin2gray -label gray /ready_empty_tb/uut/gray_count/bin2gray/gray
add wave -noupdate -expand -group {Read Empty} -expand -group {Gray Counter} -group binary_reg -label enable /ready_empty_tb/uut/gray_count/binary_reg/enable
add wave -noupdate -expand -group {Read Empty} -expand -group {Gray Counter} -group binary_reg -label D /ready_empty_tb/uut/gray_count/binary_reg/D
add wave -noupdate -expand -group {Read Empty} -expand -group {Gray Counter} -group binary_reg -label Q /ready_empty_tb/uut/gray_count/binary_reg/Q
add wave -noupdate -expand -group {Read Empty} -expand -group {Gray Counter} -group gray_reg -label enable /ready_empty_tb/uut/gray_count/gray_reg/enable
add wave -noupdate -expand -group {Read Empty} -expand -group {Gray Counter} -group gray_reg -label D /ready_empty_tb/uut/gray_count/gray_reg/D
add wave -noupdate -expand -group {Read Empty} -expand -group {Gray Counter} -group adder -label A /ready_empty_tb/uut/gray_count/add/A
add wave -noupdate -expand -group {Read Empty} -expand -group {Gray Counter} -group adder -label B /ready_empty_tb/uut/gray_count/add/B
add wave -noupdate -expand -group {Read Empty} -expand -group {Gray Counter} -group adder -label result /ready_empty_tb/uut/gray_count/add/result
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {9 ps} 0} {{Cursor 2} {1 ps} 0}
quietly wave cursor active 2
configure wave -namecolwidth 150
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {0 ps} {48 ps}
