
module wptr_full
import fifo_pkg::*;
(
    input               clk,
    input               rst,
    input  logic        inc,
    input  ptr_t        rptr,
    output addr_t       waddr,
    output ptr_t        wptr,
    output logic        full_flag
);

ptr_t wptr_graycount_w;
addr_t waddr_graycount_w;
ptr_t graynext_w;

logic not_full_or_empty_w;
logic full_flag_w;

full_fifo_ver ver_full
(
    .ptr_1(graynext_w),
    .ptr_2(rptr),
    .full_flag(full_flag_w)
);

gray_counter gray_count
(
    .clk(clk),
    .rst(rst),
    .inc(inc),
    .graynext(graynext_w),
    .not_full_or_empty(~full_flag),
    .addr(waddr_graycount_w),
    .ptr(wptr_graycount_w)
);

always_ff@(posedge clk or negedge rst) begin
    if(!rst)
        full_flag <= 1'b0;
    else
        full_flag <= full_flag_w;
end

assign waddr = waddr_graycount_w;
assign wptr = wptr_graycount_w;

endmodule
