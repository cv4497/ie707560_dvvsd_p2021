module gray_to_binary
import fifo_pkg::*;
(
    input   gray_count_t gray,
    output  gray_count_t binary
);

/* 4-bit conversion */
assign binary[W_ADDR-1] = gray[W_ADDR-1];
assign binary[W_ADDR-2] = gray[W_ADDR-1]^gray[W_ADDR-2];
assign binary[W_ADDR-3] = gray[W_ADDR-2]^gray[W_ADDR-3];
assign binary[W_ADDR-4] = gray[W_ADDR-3]^gray[W_ADDR-4];

endmodule