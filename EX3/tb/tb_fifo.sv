module fifo_wrapper
(
   // Clock signal
   input bit clk,
   // reset signal
   input bit rst_n,
   fifo_if.fifo fifo_itf
);

FIFO dut(
    .clk         ( clk          )       
   ,.rst         ( rst_n        )        
   ,.data_input  ( fifo.datain  )               
   ,.push        ( fifo.push    )         
   ,.pop         ( fifo.pop     )        
   ,.data_out    ( fifo.dataout )             
   ,.full        ( fifo.full    )         
   ,.empty       ( fifo.empty   ) 
);

endmodule

module tb_fifo();

bit clk;
bit rst_n;

class fifo_tester;
    virtual fifo_if fifo_itf;

    function new(virtual fifo_if s);
        fifo_itf = s;
    endfunction

   task init_signals();
        fifo_itf.fifo.pop = 1'b1;
        fifo_itf.fifo.pop = 1'b0;
    endtask

    task pop_data();
        fifo_itf.fifo.pop = 1'b1;
        fifo_itf.fifo.pop = 1'b0;
    endtask

    task push_data(virtual data_t data);
        fifo_itf.fifo.datain = data;
        fifo_itf.fifo.push = 1'b1;
        fifo_itf.fifo.push = 1'b0;
    endtask
endclass

fifo_tester tester;

//Instance of the DUT
fifo_wrapper dut
(
   .clk        (  clk            ),
   .rst_n     (  rst_n          ),
   .fifo_itf  (  fifo_itf.fifo  )
);

initial begin
   clk         = '0;
   rst_n       = 'd1;
   # 2 rst_n   = 'd0;
   # 2 rst_n   = 'd1;
   tester.init_signals();

   tester.push_data(8'd0);
   tester.push_data(8'd1);
   tester.push_data(8'd2);

   tester.pop_data();

   tester.pop_data();


   
   // Call your task/tasks for injecting data and push
end

initial begin
   // Call your task/tasks for pop data and review output data
end


always begin
   #1 clk <= ~clk;
end

endmodule
