
// Engineer:        Abisai Ramirez Perez 
// 
// Create Date:     June 6th, 2019
// Design Name: 
// Module Name:     dbcr_pkg
// Project Name:    debouncer
// Target Devices:  DE2-115
// Description:     This is package of the debouncer
//
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////

`ifndef DBCR_PKG_SV
    `define DBCR_PKG_SV
package dbcr_pkg;

// TRUE and FALSE definitions
localparam bit TRUE     =1'b1;
localparam bit FALSE    =1'b0;

// 
typedef enum logic [1:0]{
    BAJO = 2'b00, 
    DLY1 = 2'b01, 
    ALTO = 2'b10, 
    DLY2 = 2'b11
    } fsm_dbcr_state_e;
endpackage
`endif 

