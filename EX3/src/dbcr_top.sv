// Engineer:        DSc Abisai Ramirez Perez 
// 
// Create Date:     June 6th, 2019
// Design Name: 
// Module Name:     dbcr_top
// Project Name:    debouncer
// Target Devices:  DE2-115
// Description:     This is the top modle of the debouncer
//
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module dbcr_top(
    
    input           clk, // clk is the FPGA clock or the PLL output adjust the parameter type accordingly
    input           rst_n,  // reset low active asynchronous 
    input           Din,  // Input port 
    output logic    one_shot // One shot output port
);

logic Delay30ms_ready; 
logic EnableCounter;

fsm_dbcr  i_fsm_dbcr (
    .clk            ( clk               ),
    .rst_n          ( rst_n             ),
    .Din            ( Din               ),
    .Delay30ms_ready( Delay30ms_ready   ),
    .EnableCounter  ( EnableCounter     ),
    .one_shot       ( one_shot          ) );

cntr_mod_n_ovf #(
.FREQ(1),
.DLY(1)
) i_cntr_mod_n (
    .clk    ( clk               ),
    .rst    ( rst_n             ),
    .enb    ( EnableCounter     ),
    .ovf    ( Delay30ms_ready   ),
    .count  () );
endmodule
