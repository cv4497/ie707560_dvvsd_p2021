//Coder:          césar villarreal
//Name:           fifo_if.sv
//Description:    

`ifndef FIFO_IF_SV
    `define FIFO_IF_SV

interface fifo_if();
import sdp_sc_ram_pkg::*;

logic push;
logic pop;
logic full;
logic empty;
data_t datain;
data_t dataout;

modport fifo
(
    input datain,
    output dataout,
    output push,
    output pop,
    output full,
    output empty
);

endinterface

`endif

