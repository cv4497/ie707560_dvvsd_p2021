// Coder:       Fernanda & Omar
// Date:        November 2020
// Name:        wr_ptr.sv
// Description: This is a write pointer


module wr_ptr
import sdp_sc_ram_pkg::*;
#(parameter DW = 4)	
(
	input bit clk,
	input bit rst,
	input bit en,
	
	output logic[DW-1:0] address
);

logic[DW-1:0] temp;

always_ff @(posedge clk or negedge rst) begin
	if(!rst)
		temp <= 'd0;
	else begin
		if(en)
			temp <= temp + 'd1;
	end	
end
assign address = temp;

endmodule

